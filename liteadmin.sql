/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50639
Source Host           : localhost:3306
Source Database       : liteadmin

Target Server Type    : MYSQL
Target Server Version : 50639
File Encoding         : 65001

Date: 2019-03-27 10:51:28
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for content_advs
-- ----------------------------
DROP TABLE IF EXISTS `content_advs`;
CREATE TABLE `content_advs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cid` int(10) unsigned NOT NULL COMMENT '所属分类',
  `title` varchar(255) NOT NULL COMMENT '名称',
  `url` varchar(255) NOT NULL COMMENT '链接',
  `description` varchar(255) NOT NULL COMMENT '描述',
  `image` varchar(255) NOT NULL COMMENT '图片',
  `state` int(1) NOT NULL DEFAULT '1' COMMENT '1 启用',
  `create_time` int(11) NOT NULL,
  `is_deleted` int(1) NOT NULL DEFAULT '0' COMMENT '1 已删',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of content_advs
-- ----------------------------
INSERT INTO `content_advs` VALUES ('7', '6', 'asd as d', 'asd', 'asd', '', '1', '0', '0');
INSERT INTO `content_advs` VALUES ('8', '6', 'asdf', 'asdf', 'asdf', '', '1', '0', '1');
INSERT INTO `content_advs` VALUES ('9', '6', 'asd', 'asd', 'asd ', '', '1', '1547112582', '1');
INSERT INTO `content_advs` VALUES ('10', '0', 'asd as d', 'asd', 'asd', '', '1', '1547113047', '0');

-- ----------------------------
-- Table structure for content_advs_category
-- ----------------------------
DROP TABLE IF EXISTS `content_advs_category`;
CREATE TABLE `content_advs_category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL COMMENT '分类名称',
  `description` varchar(255) NOT NULL COMMENT '描述',
  `state` int(255) NOT NULL DEFAULT '0' COMMENT '0 禁用',
  `is_deleted` int(1) NOT NULL COMMENT '1 已删',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of content_advs_category
-- ----------------------------
INSERT INTO `content_advs_category` VALUES ('6', 'asd as d', 'asdfasdfa', '1', '0');

-- ----------------------------
-- Table structure for content_article
-- ----------------------------
DROP TABLE IF EXISTS `content_article`;
CREATE TABLE `content_article` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cid` int(11) NOT NULL COMMENT '分类ID',
  `title` varchar(255) NOT NULL COMMENT '文章标题',
  `keyword` varchar(255) NOT NULL COMMENT '文章关键词',
  `description` varchar(255) NOT NULL COMMENT '文章描述',
  `thumb` varchar(255) NOT NULL COMMENT '文章缩略图',
  `content` longtext NOT NULL COMMENT '文章html正文',
  `md_content` longtext NOT NULL COMMENT '文章markdown正文',
  `create_time` int(11) NOT NULL COMMENT '文章创建时间',
  `update_time` int(11) NOT NULL COMMENT '文章修改时间',
  `is_deleted` tinyint(1) unsigned NOT NULL COMMENT '删除位',
  `state` tinyint(1) unsigned NOT NULL COMMENT '显示隐藏',
  `click` int(11) NOT NULL COMMENT '点击量',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='文章表';

-- ----------------------------
-- Records of content_article
-- ----------------------------
INSERT INTO `content_article` VALUES ('1', '4', 'linux系统的关机命令', 'linux，命令行', '这里介绍几种常用的linux系统关机命令', '', '<ul>\n<li>halt</li>\n<li>init 0</li>\n<li>shutdown -h now</li>\n</ul>', '- halt\r\n- init 0\r\n- shutdown -h now\r\n\r\n', '1537929187', '1537931249', '0', '1', '4');
INSERT INTO `content_article` VALUES ('2', '5', '爬虫需谨慎，你不知道的爬虫与反爬虫套路！', '爬虫', '爬虫与反爬虫，是一个很不阳光的行业。这里说的不阳光，有两个含义。', '', '<p>第一是，这个行业是隐藏在地下的，一般很少被曝光出来。很多公司对外都不会宣称自己有爬虫团队，甚至隐瞒自己有反爬虫团队的事实。这可能是出于公司战略角度来看的，与技术无关。</p>\n<p>第二是，这个行业并不是一个很积极向上的行业。很多人在这个行业摸爬滚打了多年，积攒了大量的经验，但是悲哀的发现，这些经验很难兑换成闪光的简历。</p>\n<p>面试的时候，因为双方爬虫理念或者反爬虫理念不同，也很可能互不认可，影响自己的求职之路。本来程序员就有“文人相轻”的倾向，何况理念真的大不同。</p>\n<p>然而这就是程序员的宿命。不管这个行业有多么的不阳光，依然无法阻挡大量的人进入这个行业，因为有公司的需求。</p>', '第一是，这个行业是隐藏在地下的，一般很少被曝光出来。很多公司对外都不会宣称自己有爬虫团队，甚至隐瞒自己有反爬虫团队的事实。这可能是出于公司战略角度来看的，与技术无关。\r\n\r\n第二是，这个行业并不是一个很积极向上的行业。很多人在这个行业摸爬滚打了多年，积攒了大量的经验，但是悲哀的发现，这些经验很难兑换成闪光的简历。\r\n\r\n面试的时候，因为双方爬虫理念或者反爬虫理念不同，也很可能互不认可，影响自己的求职之路。本来程序员就有“文人相轻”的倾向，何况理念真的大不同。\r\n\r\n然而这就是程序员的宿命。不管这个行业有多么的不阳光，依然无法阻挡大量的人进入这个行业，因为有公司的需求。\r\n\r\n', '1537929392', '1537931502', '0', '1', '11');
INSERT INTO `content_article` VALUES ('3', '0', 'Swoole：面向生产环境的 PHP 异步网络通信引擎', 'swoole', '面向生产环境的 PHP 异步网络通信引擎', '/uploads/20180926/ab4669ac2af7cbc0ede45fd0a14a20e0.png', '<p>使 PHP 开发人员可以编写高性能的异步并发 TCP、UDP、Unix Socket、HTTP，WebSocket 服务。Swoole 可以广泛应用于互联网、移动通信、企业软件、云计算、网络游戏、物联网（IOT）、车联网、智能家居等领域。 使用 PHP + Swoole 作为网络通信框架，可以使企业 IT 研发团队的效率大大提升，更加专注于开发创新产品。</p>', '使 PHP 开发人员可以编写高性能的异步并发 TCP、UDP、Unix Socket、HTTP，WebSocket 服务。Swoole 可以广泛应用于互联网、移动通信、企业软件、云计算、网络游戏、物联网（IOT）、车联网、智能家居等领域。 使用 PHP + Swoole 作为网络通信框架，可以使企业 IT 研发团队的效率大大提升，更加专注于开发创新产品。', '1537929422', '1537931687', '0', '1', '23');

-- ----------------------------
-- Table structure for content_article_reply
-- ----------------------------
DROP TABLE IF EXISTS `content_article_reply`;
CREATE TABLE `content_article_reply` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL COMMENT '文章ID',
  `ip` int(11) NOT NULL COMMENT '留言IP',
  `name` varchar(25) NOT NULL COMMENT '留言者姓名',
  `content` varchar(255) NOT NULL COMMENT '留言内容',
  `qq` char(10) NOT NULL COMMENT '留言者QQ',
  `website` varchar(255) NOT NULL COMMENT '网站地址',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `state` tinyint(1) NOT NULL COMMENT '审核状态 1过审 0 待审',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='文章评论表';

-- ----------------------------
-- Records of content_article_reply
-- ----------------------------

-- ----------------------------
-- Table structure for content_attachment
-- ----------------------------
DROP TABLE IF EXISTS `content_attachment`;
CREATE TABLE `content_attachment` (
  `hash` char(32) NOT NULL COMMENT '附件文件MD5',
  `path` varchar(255) NOT NULL COMMENT '附件存放路径',
  `create_time` int(11) NOT NULL COMMENT '文件上传时间',
  `size` int(11) unsigned NOT NULL COMMENT '文件大小',
  PRIMARY KEY (`hash`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='上传文件表';

-- ----------------------------
-- Records of content_attachment
-- ----------------------------
INSERT INTO `content_attachment` VALUES ('06cde5906b787a58200dd8c13bbffb28', '/uploads/20180926/ab4669ac2af7cbc0ede45fd0a14a20e0.png', '1537931649', '32492');
INSERT INTO `content_attachment` VALUES ('37bcfade3025ef31b13827b005c69704', '/uploads/20180814/b1c4adacbc7289868fa33d071e6a455b.png', '1534240210', '190184');
INSERT INTO `content_attachment` VALUES ('3c8961c92429c5a92cc44ae2b7dfce01', '/uploads/20180814/43a5f5c1010b4811aeebe2ca247bfac4.png', '1534240151', '1616');
INSERT INTO `content_attachment` VALUES ('64d37eca7d4ec3c70ba27103a77f32ff', '/uploads/20180814/693db0bd37e80801bd431449ad112ae6.jpg', '1534240160', '152586');
INSERT INTO `content_attachment` VALUES ('72bae0474d2106c1642b03aaaa492d82', '/uploads/20180926/24dac561d3c452e04980d5a4c7fe65b9.jpg', '1537930751', '2535');
INSERT INTO `content_attachment` VALUES ('fe134da6f0c7fac92184a0db36ce834c', '/uploads/20180814/3a424041ca5c6d0c5128df1901eceefb.png', '1534239731', '31503');

-- ----------------------------
-- Table structure for content_category
-- ----------------------------
DROP TABLE IF EXISTS `content_category`;
CREATE TABLE `content_category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '上级分类ID',
  `title` varchar(255) NOT NULL COMMENT '分类名称',
  `seo_keyword` varchar(255) NOT NULL COMMENT 'seo 关键词',
  `seo_description` varchar(255) NOT NULL COMMENT 'seo描述',
  `path` varchar(255) NOT NULL COMMENT '树形路径',
  `theme` varchar(255) NOT NULL COMMENT '模板',
  `is_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除',
  `state` tinyint(1) unsigned NOT NULL COMMENT '显示隐藏',
  `content` text NOT NULL COMMENT '分类内容',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='文章分类表';

-- ----------------------------
-- Records of content_category
-- ----------------------------
INSERT INTO `content_category` VALUES ('4', '0', 'linux', 'linux', 'linux系统学习', '0,4', 'index', '0', '0', '<p><img src=\"/uploads/20180926/24dac561d3c452e04980d5a4c7fe65b9.jpg\" alt=\"24dac561d3c452e04980d5a4c7fe65b9.jpg\" /><br /></p>');
INSERT INTO `content_category` VALUES ('5', '0', '后端开发', '后端代码开发', '后端代码开发', '0,5', 'index', '0', '1', '<p><br /></p>');
INSERT INTO `content_category` VALUES ('6', '5', 'PHP', 'PHP语言开发', 'PHP语言开发', '0,5,6', 'index', '0', '0', '<p>PHP语言开发</p>');

-- ----------------------------
-- Table structure for content_link
-- ----------------------------
DROP TABLE IF EXISTS `content_link`;
CREATE TABLE `content_link` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL COMMENT '显示名称',
  `href` varchar(255) NOT NULL COMMENT '链接地址',
  `state` tinyint(1) unsigned NOT NULL COMMENT '启用状态 1 是 0 否',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='友情链接表';

-- ----------------------------
-- Records of content_link
-- ----------------------------
INSERT INTO `content_link` VALUES ('2', '我的博客', 'http://blog.dazhetu.cn', '1');

-- ----------------------------
-- Table structure for content_music
-- ----------------------------
DROP TABLE IF EXISTS `content_music`;
CREATE TABLE `content_music` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL COMMENT '歌曲名',
  `author` varchar(255) NOT NULL COMMENT '歌手名',
  `url` varchar(255) NOT NULL COMMENT '音乐链接',
  `pic` varchar(255) NOT NULL COMMENT '音乐封面',
  `lrc` text NOT NULL COMMENT '歌词地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='音乐表';

-- ----------------------------
-- Records of content_music
-- ----------------------------
INSERT INTO `content_music` VALUES ('3', '一百万个可能', '歌手：Christine Welch。所属专辑：一百万个可能。', 'http://music.163.com/song/media/outer/url?id=29722582.mp3', 'http://p1.music.126.net/SSGt30LAVJwW31-qreZDFA==/2532175280446455.jpg?param=130y130', '[00:18.19]幽静窗外满地片片寒花\n[00:24.66]一瞬间永恒的时差\n[00:29.61]窝在棉被里\n[00:35.59]倾听踏雪听沉默的声音\n[00:42.09]飘雪藏永恒的身影\n[00:47.40]雪树下等你\n[00:48.63]\n[00:53.70]在一瞬间有一百万个可能\n[00:56.10]该向前走或者继续等\n[01:00.46]这冬夜里有百万个不确定\n[01:04.87]渐入深夜或期盼天明\n[01:09.32]云空的泪一如冰凌结晶了\n[01:15.69]成雪花垂\n[01:18.01]这一瞬间有一百万个可能\n[01:21.57]窝进棉被或面对寒冷\n[01:28.22]\n[01:45.48]幽静寒风吹来一缕声音\n[01:51.97]一瞬间看着你走近\n[01:56.64]暖了我冬心\n[02:02.84]倾听踏雪听沉默的声音\n[02:09.32]飘雪藏永恒的身影\n[02:14.32]雪树下等你\n[02:16.29]\n[02:19.13]在一瞬间有一百万个可能\n[02:23.46]该向前走或者继续等\n[02:27.72]这冬夜里有百万个不确定\n[02:32.31]渐入深夜或期盼天明\n[02:36.15]云空的泪一如冰凌结晶了\n[02:43.02]成雪花垂\n[02:45.24]这一瞬间有一百万个可能\n[02:49.64]窝进棉被或面对寒冷\n[02:55.35]\n[03:13.69]那晚上会是哪个瞬间\n[03:15.04]说好的爱会不会改变\n[03:17.01]而你让我徘徊在千里之外\n[03:18.88]yeah你让我等了好久baby\n[03:21.56]突然间那是哪个瞬间\n[03:23.69]你终於出现就是那个瞬间\n[03:25.90]等了好久忍不住伸手那个瞬间\n[03:28.90]\n[03:29.76]在一瞬间有一百万个可能\n[03:33.32]该向前走或者继续等\n[03:37.48]这深夜里有百万个不确定\n[03:42.00]渐入冬林或走向街灯\n[03:46.15]云空的泪一如冰凌结晶了\n[03:53.74]成雪花垂\n[03:55.63]\n[03:56.03]这一瞬间有一百万个可能\n[03:59.57]暖这冬心或面对寒冷\n[04:03.89]该向前走或者继续等\n[04:08.18]渐入冬林或走向街灯\n[04:12.62]窝进棉被或面对寒冷\n[04:16.93]暖这冬心或面对寒冷\n[04:24.52]\n');
INSERT INTO `content_music` VALUES ('4', '担风袖月', '歌手：小魂。所属专辑：担风袖月。', 'http://music.163.com/song/media/outer/url?id=433103629.mp3', 'http://p1.music.126.net/utNW91y3WTL6CST_55kerQ==/18171628673024640.jpg?param=130y130', '[by:一寸云间_]\n[ti:担风袖月]\n[ar:小魂]\n[al:]\n[by:九九Lrc歌词网～www.99Lrc.net]\n[00:00.00] 作曲 : 烧焦的鱼\n[00:00.698] 作词 : 瘦尽灯花\n[00:02.95]策:端木薯＆茶园\n[00:05.80]编/和声:烧焦的鱼\n[00:11.11]唱:小魂\n[00:13.84]后期:凪\n[00:16.41]美工:胖次\n[00:19.01]\n[00:31.72]青崖静水沉 暮色独斟\n[00:38.24]树影婆娑枕 梵声沌\n[00:45.28]涧芳远苔润 偃卧旅人\n[00:52.21]天地好峥嵘 劈哀神 宿雨无门\n[00:59.14]九州风月存 入我芳樽\n[01:06.00]孤影写泥尘 云岚深\n[01:12.96]眉尾走驳唇 峰间解春\n[01:19.84]倚杖踏素风痕 霜虹落刃\n[01:25.95]昔日五更伏案 金翅华衫\n[01:32.84]奉折额叩高殿 诏谕隐谋权\n[01:39.71]朝野朋党皆横乱 浊目昏瞻\n[01:46.82]不如归梦河山 徒衰老鬓端\n[01:53.68]曾见鱼龙虎豺 浪子庸才\n[02:00.56]紫蟒袍虽难裁 不及诗三百\n[02:07.51]今我心如飞絮白 知交倾盖\n[02:14.48]当时明月尚在 两袖清风来\n[02:23.12]\n[02:29.47]草木携灵气 霜雪不欺\n[02:36.04]拥鹤子梅妻 甩墨讥\n[02:42.89]慷慨长剑系 皓月丹心\n[02:49.95]忆晏晏宾座 所余几 且看如今\n[02:56.96]夜棹武陵溪 衣薄屐轻\n[03:03.77]醉卧桃源里 不堪醒\n[03:10.65]寻蓬莱归去 潦寥拙笔\n[03:17.44]莫笑我癫狂意 酣眠天地\n[03:23.69]昔日五更伏案 金翅华衫\n[03:30.57]奉折额叩高殿 诏谕隐谋权\n[03:37.80]朝野朋党皆横乱 浊目昏瞻\n[03:44.36]不如归梦河山 徒衰老鬓端\n[03:51.29]曾见鱼龙虎豺 浪子庸才\n[03:58.30]紫蟒袍虽难裁 不及诗三百\n[04:05.19]今我心如飞絮白 知交倾盖\n[04:12.21]当时明月尚在 两袖清风来\n[04:20.08]“明月在，清风来。”\n[04:28.30]\n');

-- ----------------------------
-- Table structure for content_tags
-- ----------------------------
DROP TABLE IF EXISTS `content_tags`;
CREATE TABLE `content_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag` varchar(255) NOT NULL COMMENT '标签',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='文章标签表';

-- ----------------------------
-- Records of content_tags
-- ----------------------------
INSERT INTO `content_tags` VALUES ('3', 'linux');
INSERT INTO `content_tags` VALUES ('4', '命令行');
INSERT INTO `content_tags` VALUES ('5', '爬虫');
INSERT INTO `content_tags` VALUES ('6', 'swoole');

-- ----------------------------
-- Table structure for content_tags_map
-- ----------------------------
DROP TABLE IF EXISTS `content_tags_map`;
CREATE TABLE `content_tags_map` (
  `tag_id` int(10) NOT NULL COMMENT '标签ID',
  `article_id` int(10) NOT NULL COMMENT '文章ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文章标签关系表';

-- ----------------------------
-- Records of content_tags_map
-- ----------------------------
INSERT INTO `content_tags_map` VALUES ('3', '1');
INSERT INTO `content_tags_map` VALUES ('4', '1');
INSERT INTO `content_tags_map` VALUES ('5', '2');
INSERT INTO `content_tags_map` VALUES ('6', '3');

-- ----------------------------
-- Table structure for site_config
-- ----------------------------
DROP TABLE IF EXISTS `site_config`;
CREATE TABLE `site_config` (
  `name` varchar(255) NOT NULL COMMENT '配置名',
  `value` varchar(255) NOT NULL COMMENT '配置值',
  `label` varchar(255) NOT NULL COMMENT '显示名称',
  PRIMARY KEY (`name`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='站点配置表';

-- ----------------------------
-- Records of site_config
-- ----------------------------
INSERT INTO `site_config` VALUES ('site_cip', '冀IPC备0000001', '备案号');
INSERT INTO `site_config` VALUES ('site_name', '我的网站', '网站名称');

-- ----------------------------
-- Table structure for system_admin
-- ----------------------------
DROP TABLE IF EXISTS `system_admin`;
CREATE TABLE `system_admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(25) NOT NULL COMMENT '用户名',
  `password` varchar(255) NOT NULL COMMENT '密码',
  `name` varchar(10) NOT NULL COMMENT '姓名',
  `create_time` int(11) unsigned NOT NULL COMMENT '创建时间',
  `last_login` int(11) unsigned NOT NULL COMMENT '上次登录时间',
  `is_deleted` int(11) NOT NULL COMMENT '删除位',
  `state` int(11) NOT NULL DEFAULT '1' COMMENT '启用状态',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='后台管理员表';

-- ----------------------------
-- Records of system_admin
-- ----------------------------
INSERT INTO `system_admin` VALUES ('1', 'admin', '$2y$11$iR/m.ctNLiS/0g9SkPDE.OElmV6pTP.En79wF7JgQLQt2RXUNjiwa', '超级管理员', '0', '1553651928', '0', '1');
INSERT INTO `system_admin` VALUES ('2', 'bianji', '$2y$11$8jV2bGk6xqBvmsTWXfZ2IO9ij0yaB3YDxirv.56ZlNmfaO8wYs8vK', '测试编辑', '1527521448', '1553591966', '0', '1');

-- ----------------------------
-- Table structure for system_auth_map
-- ----------------------------
DROP TABLE IF EXISTS `system_auth_map`;
CREATE TABLE `system_auth_map` (
  `admin_id` int(11) NOT NULL COMMENT '用户ID',
  `role_id` int(11) NOT NULL COMMENT '角色ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户角色对应关系表';

-- ----------------------------
-- Records of system_auth_map
-- ----------------------------
INSERT INTO `system_auth_map` VALUES ('1', '2');
INSERT INTO `system_auth_map` VALUES ('1', '3');
INSERT INTO `system_auth_map` VALUES ('2', '3');

-- ----------------------------
-- Table structure for system_auth_node
-- ----------------------------
DROP TABLE IF EXISTS `system_auth_node`;
CREATE TABLE `system_auth_node` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '权限名称',
  `path` varchar(255) NOT NULL COMMENT '权限uri',
  `auth` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0不限制 1登录 2授权',
  `level` int(1) unsigned NOT NULL COMMENT '权限层级',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=175 DEFAULT CHARSET=utf8 COMMENT='权限节点表';

-- ----------------------------
-- Records of system_auth_node
-- ----------------------------
INSERT INTO `system_auth_node` VALUES ('28', 'admin', 'admin', '0', '1');
INSERT INTO `system_auth_node` VALUES ('29', '后台主页', 'admin/index', '0', '2');
INSERT INTO `system_auth_node` VALUES ('30', '后台框架首页', 'admin/index/index', '0', '3');
INSERT INTO `system_auth_node` VALUES ('32', '菜单管理', 'admin/menu', '0', '2');
INSERT INTO `system_auth_node` VALUES ('33', '列表页', 'admin/menu/index', '2', '3');
INSERT INTO `system_auth_node` VALUES ('34', '权限节点', 'admin/node', '0', '2');
INSERT INTO `system_auth_node` VALUES ('35', '列表', 'admin/node/index', '2', '3');
INSERT INTO `system_auth_node` VALUES ('37', '刷新节点', 'admin/node/clear', '2', '3');
INSERT INTO `system_auth_node` VALUES ('41', '添加', 'admin/menu/add', '2', '3');
INSERT INTO `system_auth_node` VALUES ('43', '修改', 'admin/menu/edit', '2', '3');
INSERT INTO `system_auth_node` VALUES ('44', '删除', 'admin/menu/del', '2', '3');
INSERT INTO `system_auth_node` VALUES ('45', '角色管理', 'admin/role', '0', '2');
INSERT INTO `system_auth_node` VALUES ('46', '列表页', 'admin/role/index', '2', '3');
INSERT INTO `system_auth_node` VALUES ('47', '添加', 'admin/role/add', '2', '3');
INSERT INTO `system_auth_node` VALUES ('48', '编辑', 'admin/role/edit', '2', '3');
INSERT INTO `system_auth_node` VALUES ('49', '授权', 'admin/role/access', '2', '3');
INSERT INTO `system_auth_node` VALUES ('50', '删除及批量删除', 'admin/role/del', '2', '3');
INSERT INTO `system_auth_node` VALUES ('51', '后台管理员', 'admin/admin', '0', '2');
INSERT INTO `system_auth_node` VALUES ('52', '管理员列表', 'admin/admin/index', '2', '3');
INSERT INTO `system_auth_node` VALUES ('53', '添加操作', 'admin/admin/add', '2', '3');
INSERT INTO `system_auth_node` VALUES ('54', '编辑操作', 'admin/admin/edit', '2', '3');
INSERT INTO `system_auth_node` VALUES ('55', '删除及批量删除', 'admin/admin/del', '2', '3');
INSERT INTO `system_auth_node` VALUES ('56', '修改密码', 'admin/admin/password', '2', '3');
INSERT INTO `system_auth_node` VALUES ('57', '角色授权', 'admin/admin/role', '2', '3');
INSERT INTO `system_auth_node` VALUES ('58', '禁用/启用', 'admin/admin/change', '2', '3');
INSERT INTO `system_auth_node` VALUES ('60', '登陆', 'admin/login', '0', '2');
INSERT INTO `system_auth_node` VALUES ('61', '登陆操作', 'admin/login/login', '2', '3');
INSERT INTO `system_auth_node` VALUES ('69', '退出操作', 'admin/login/logout', '2', '3');
INSERT INTO `system_auth_node` VALUES ('84', '文件上传', 'admin/upload', '0', '2');
INSERT INTO `system_auth_node` VALUES ('85', '文件上传', 'admin/upload/file', '1', '3');
INSERT INTO `system_auth_node` VALUES ('86', '检查图片是否存在', 'admin/upload/checkfile', '1', '3');
INSERT INTO `system_auth_node` VALUES ('87', '百度umeditor富文本编辑器图片插入', 'admin/upload/ueditor', '1', '3');
INSERT INTO `system_auth_node` VALUES ('102', 'wangEditor富文本编辑器图片插入', 'admin/upload/wangeditor', '1', '3');
INSERT INTO `system_auth_node` VALUES ('106', 'markdown编辑器图片插入', 'admin/upload/markdown', '1', '3');
INSERT INTO `system_auth_node` VALUES ('108', '修改密码', 'admin/index/password', '2', '3');
INSERT INTO `system_auth_node` VALUES ('118', '文章评论', 'admin/content.reply', '0', '2');
INSERT INTO `system_auth_node` VALUES ('119', '列表页', 'admin/content.reply/index', '2', '3');
INSERT INTO `system_auth_node` VALUES ('120', '禁用/启用', 'admin/content.reply/change', '2', '3');
INSERT INTO `system_auth_node` VALUES ('121', '删除操作', 'admin/content.reply/del', '2', '3');
INSERT INTO `system_auth_node` VALUES ('122', '文章管理', 'admin/content.article', '0', '2');
INSERT INTO `system_auth_node` VALUES ('123', '列表页', 'admin/content.article/index', '2', '3');
INSERT INTO `system_auth_node` VALUES ('124', '添加操作', 'admin/content.article/add', '2', '3');
INSERT INTO `system_auth_node` VALUES ('125', '编辑操作', 'admin/content.article/edit', '2', '3');
INSERT INTO `system_auth_node` VALUES ('126', '删除', 'admin/content.article/del', '2', '3');
INSERT INTO `system_auth_node` VALUES ('127', '禁用/启用', 'admin/content.article/change', '2', '3');
INSERT INTO `system_auth_node` VALUES ('128', '重建索引', 'admin/content.article/rebuild', '2', '3');
INSERT INTO `system_auth_node` VALUES ('141', '标签管理', 'admin/content.tags', '0', '2');
INSERT INTO `system_auth_node` VALUES ('142', '列表页', 'admin/content.tags/index', '2', '3');
INSERT INTO `system_auth_node` VALUES ('143', '删除操作', 'admin/content.tags/del', '2', '3');
INSERT INTO `system_auth_node` VALUES ('144', '友情链接', 'admin/content.link', '0', '2');
INSERT INTO `system_auth_node` VALUES ('145', '列表页', 'admin/content.link/index', '2', '3');
INSERT INTO `system_auth_node` VALUES ('146', '添加', 'admin/content.link/add', '2', '3');
INSERT INTO `system_auth_node` VALUES ('147', '编辑操作', 'admin/content.link/edit', '2', '3');
INSERT INTO `system_auth_node` VALUES ('148', '删除操作', 'admin/content.link/del', '2', '3');
INSERT INTO `system_auth_node` VALUES ('149', '禁用/启用', 'admin/content.link/change', '2', '3');
INSERT INTO `system_auth_node` VALUES ('150', '分类管理', 'admin/content.category', '0', '2');
INSERT INTO `system_auth_node` VALUES ('151', '列表页', 'admin/content.category/index', '2', '3');
INSERT INTO `system_auth_node` VALUES ('152', '添加操作', 'admin/content.category/add', '2', '3');
INSERT INTO `system_auth_node` VALUES ('153', '编辑操作', 'admin/content.category/edit', '2', '3');
INSERT INTO `system_auth_node` VALUES ('154', '删除操作', 'admin/content.category/del', '2', '3');
INSERT INTO `system_auth_node` VALUES ('155', '禁用/启用', 'admin/content.category/change', '2', '3');
INSERT INTO `system_auth_node` VALUES ('156', '音乐管理', 'admin/content.music', '0', '2');
INSERT INTO `system_auth_node` VALUES ('157', '列表页', 'admin/content.music/index', '2', '3');
INSERT INTO `system_auth_node` VALUES ('158', '添加歌曲', 'admin/content.music/add', '2', '3');
INSERT INTO `system_auth_node` VALUES ('159', '删除歌曲', 'admin/content.music/del', '2', '3');
INSERT INTO `system_auth_node` VALUES ('160', '修改个人信息', 'admin/index/editprofile', '2', '3');
INSERT INTO `system_auth_node` VALUES ('161', '仪表盘', 'admin/index/dashboard', '0', '3');
INSERT INTO `system_auth_node` VALUES ('162', '清理缓存', 'admin/index/clearcache', '1', '3');
INSERT INTO `system_auth_node` VALUES ('163', '广告分类', 'admin/content.advs_category', '0', '2');
INSERT INTO `system_auth_node` VALUES ('164', '列表页', 'admin/content.advs_category/index', '2', '3');
INSERT INTO `system_auth_node` VALUES ('165', '添加操作', 'admin/content.advs_category/add', '2', '3');
INSERT INTO `system_auth_node` VALUES ('166', '编辑操作', 'admin/content.advs_category/edit', '2', '3');
INSERT INTO `system_auth_node` VALUES ('167', '禁用/启用', 'admin/content.advs_category/change', '2', '3');
INSERT INTO `system_auth_node` VALUES ('168', '删除操作', 'admin/content.advs_category/del', '2', '3');
INSERT INTO `system_auth_node` VALUES ('169', '广告管理', 'admin/content.advs', '0', '2');
INSERT INTO `system_auth_node` VALUES ('170', '列表页', 'admin/content.advs/index', '2', '3');
INSERT INTO `system_auth_node` VALUES ('171', '添加操作', 'admin/content.advs/add', '2', '3');
INSERT INTO `system_auth_node` VALUES ('172', '编辑操作', 'admin/content.advs/edit', '2', '3');
INSERT INTO `system_auth_node` VALUES ('173', '删除操作', 'admin/content.advs/del', '2', '3');
INSERT INTO `system_auth_node` VALUES ('174', '启用/禁用', 'admin/content.advs/change', '2', '3');

-- ----------------------------
-- Table structure for system_menu
-- ----------------------------
DROP TABLE IF EXISTS `system_menu`;
CREATE TABLE `system_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(20) NOT NULL COMMENT '菜单名称',
  `url` varchar(255) NOT NULL COMMENT '菜单链接',
  `icon` varchar(255) NOT NULL DEFAULT '' COMMENT '菜单图标',
  `pid` int(11) NOT NULL COMMENT '上级节点ID',
  `sort` int(11) NOT NULL COMMENT '排序',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '启用状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COMMENT='系统菜单表';

-- ----------------------------
-- Records of system_menu
-- ----------------------------
INSERT INTO `system_menu` VALUES ('1', '系统核心', '#', '', '0', '50', '1');
INSERT INTO `system_menu` VALUES ('2', '系统设置', '#', '', '1', '50', '1');
INSERT INTO `system_menu` VALUES ('3', '权限节点', 'admin/node/index', '', '2', '50', '1');
INSERT INTO `system_menu` VALUES ('4', '菜单设置', 'admin/menu/index', '', '2', '50', '1');
INSERT INTO `system_menu` VALUES ('9', '角色授权', 'admin/role/index', '', '2', '50', '1');
INSERT INTO `system_menu` VALUES ('10', '后台用户', 'admin/admin/index', '', '2', '50', '1');
INSERT INTO `system_menu` VALUES ('11', '内容门户', '#', '', '0', '50', '0');
INSERT INTO `system_menu` VALUES ('12', '内容管理', '#', '', '11', '50', '0');
INSERT INTO `system_menu` VALUES ('13', '分类管理', 'admin/content.category/index', '', '12', '50', '0');
INSERT INTO `system_menu` VALUES ('14', '文章管理', 'admin/content.article/index', '', '12', '50', '0');
INSERT INTO `system_menu` VALUES ('15', '友情链接', 'admin/content.link/index', '', '11', '50', '0');
INSERT INTO `system_menu` VALUES ('16', '广告位管理', '#', '', '11', '50', '0');
INSERT INTO `system_menu` VALUES ('17', '广告分类', 'admin/content.advs_category/index', '', '16', '50', '0');
INSERT INTO `system_menu` VALUES ('18', '广告链接', 'admin/content.advs/index', '', '16', '50', '0');
INSERT INTO `system_menu` VALUES ('21', '标签管理', 'admin/content.tags/index', '', '12', '50', '0');
INSERT INTO `system_menu` VALUES ('22', '音乐管理', 'admin/content.music/index', '', '11', '50', '0');
INSERT INTO `system_menu` VALUES ('23', '文章评论', 'admin/content.reply/index', '', '11', '50', '0');

-- ----------------------------
-- Table structure for system_role
-- ----------------------------
DROP TABLE IF EXISTS `system_role`;
CREATE TABLE `system_role` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(20) NOT NULL COMMENT '名称',
  `access_list` varchar(255) NOT NULL DEFAULT '' COMMENT '权限列表',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='角色权限集';

-- ----------------------------
-- Records of system_role
-- ----------------------------
INSERT INTO `system_role` VALUES ('2', '管理员', '53,58,55,54,52,56,57,101,41,44,43,33,37,35,49,47,50,48,46');
INSERT INTO `system_role` VALUES ('3', '网站编辑', '171,174,173,172,170,165,167,168,166,164,124,127,126,125,123,128,152,155,154,153,151,146,149,148,147,145,158,159,157,120,121,119,143,142');

-- ----------------------------
-- Table structure for verydows_order_log
-- ----------------------------
DROP TABLE IF EXISTS `verydows_order_log`;
CREATE TABLE `verydows_order_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` char(15) NOT NULL DEFAULT '',
  `admin_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `operate` char(10) NOT NULL,
  `cause` varchar(240) NOT NULL DEFAULT '',
  `dateline` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `order_id` (`order_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of verydows_order_log
-- ----------------------------
